package com.ordering.system.pulsar.config;

import lombok.Data;
import org.apache.pulsar.client.api.CompressionType;
import org.apache.pulsar.client.api.HashingScheme;
import org.apache.pulsar.client.api.MessageRoutingMode;
import org.apache.pulsar.client.api.ProducerCryptoFailureAction;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "pulsar-producer-config")
public class PulsarProducerConfigData {

    private String topicName;
    private boolean batchingEnabled;
    private int batchingMaxMessages;
    private long batchingMaxPublishDelayMicros;
    private boolean blockIfQueueFull;
    private boolean chunkingEnabled;
    private CompressionType compressionType;
    private ProducerCryptoFailureAction cryptoFailureAction;
    private HashingScheme hashingScheme;
    private String initialSubscriptionName;
    private int maxPendingMessage;
    private int maxPendingMessagesAcrossPartitions;
    private MessageRoutingMode messageRoutingMode;
    private String producerName;
    private long sendTimeoutMs;

}
