package com.ordering.system.pulsar.consumer.service;

public interface PulsarConsumer<T>{
    void receive(T message);
}
