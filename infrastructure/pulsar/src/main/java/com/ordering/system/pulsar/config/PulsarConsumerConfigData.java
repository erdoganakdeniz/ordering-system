package com.ordering.system.pulsar.config;

import lombok.Data;
import org.apache.pulsar.client.api.SubscriptionType;
import org.apache.pulsar.common.schema.SchemaType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;



@Data
@Configuration
@ConfigurationProperties(prefix = "pulsar-consumer-config")
public class PulsarConsumerConfigData {

    private String[] topics;
    private String subscriptionName;
    private String consumerName;
    private SubscriptionType subscriptionType=SubscriptionType.Shared;
    private SchemaType schemaType=SchemaType.JSON;




}
