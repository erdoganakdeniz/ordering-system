package com.ordering.system.pulsar.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;


@Data
@Configuration
@ConfigurationProperties(prefix = "pulsar-config")
public class PulsarConfigData {

    private String serviceUrl;
    private Integer ioThreads = 10;
    private String listenerName;
    private Integer listenerThreads = 10;
    private boolean enableTcpNoDelay = false;
    private Integer keepAliveIntervalSec = 20;
    private Integer connectionTimeoutSec = 10;
    private Integer operationTimeoutSec = 15;
    private Integer startingBackoffIntervalMs = 100;
    private Integer maxBackoffIntervalSec = 10;
    private String consumerNameDelimiter = "";
    private String namespace = "default";
    private String tenant = "public";
    private String tlsTrustCertsFilePath = null;
    private Set<String> tlsCiphers = new HashSet<>();
    private Set<String> tlsProtocols = new HashSet<>();
    private String tlsTrustStorePassword = null;
    private String tlsTrustStorePath = null;
    private String tlsTrustStoreType = null;
    private boolean useKeyStoreTls = false;
    private boolean allowTlsInsecureConnection = false;
    private boolean enableTlsHostnameVerification = false;
    private String tlsAuthCertFilePath = null;
    private String tlsAuthKeyFilePath = null;
    private String tokenAuthValue = null;
    private String oauth2IssuerUrl = null;
    private String oauth2CredentialsUrl = null;
    private String oauth2Audience = null;



}
