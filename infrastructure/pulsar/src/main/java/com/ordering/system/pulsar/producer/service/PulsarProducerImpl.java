package com.ordering.system.pulsar.producer.service;

import com.ordering.system.pulsar.config.PulsarProducerConfigData;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.pulsar.core.PulsarTemplate;


public class PulsarProducerImpl<T> implements PulsarProducer<T>{

    private final PulsarProducerConfigData producerConfigData;
    private final PulsarTemplate<T> template;

    public PulsarProducerImpl(PulsarProducerConfigData producerConfigData, PulsarTemplate<T> template) {
        this.producerConfigData = producerConfigData;
        this.template = template;
    }

    @Override
    public void publish(T message) {
        try {


            template.send(producerConfigData.getTopicName(), message);

        } catch (PulsarClientException e) {
            e.printStackTrace();
        }


    }
}
