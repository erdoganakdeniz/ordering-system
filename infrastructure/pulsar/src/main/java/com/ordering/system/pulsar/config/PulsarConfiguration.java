package com.ordering.system.pulsar.config;

import com.ordering.system.pulsar.exception.ClientException;
import org.apache.pulsar.client.api.ClientBuilder;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;


@Configuration
@ComponentScan
@EnableConfigurationProperties
public class PulsarConfiguration {
    private final PulsarConfigData configData;

    public PulsarConfiguration(PulsarConfigData configData) {
        this.configData = configData;
    }

    @Bean
    @ConditionalOnMissingBean
    public PulsarClient pulsarClient() throws PulsarClientException {

        final ClientBuilder pulsarClientBuilder = PulsarClient.builder()
                .listenerName(configData.getListenerName())
                .serviceUrl(configData.getServiceUrl())
                .ioThreads(configData.getIoThreads())
                .listenerThreads(configData.getListenerThreads())
                .enableTcpNoDelay(configData.isEnableTcpNoDelay())
                .keepAliveInterval(configData.getKeepAliveIntervalSec(), TimeUnit.SECONDS)
                .connectionTimeout(configData.getConnectionTimeoutSec(), TimeUnit.SECONDS)
                .operationTimeout(configData.getOperationTimeoutSec(), TimeUnit.SECONDS)
                .startingBackoffInterval(configData.getStartingBackoffIntervalMs(), TimeUnit.MILLISECONDS)
                .maxBackoffInterval(configData.getMaxBackoffIntervalSec(), TimeUnit.SECONDS)
                .useKeyStoreTls(configData.isUseKeyStoreTls())
                .tlsTrustCertsFilePath(configData.getTlsTrustCertsFilePath())
                .tlsCiphers(configData.getTlsCiphers())
                .tlsProtocols(configData.getTlsProtocols())
                .tlsTrustStorePassword(configData.getTlsTrustStorePassword())
                .tlsTrustStorePath(configData.getTlsTrustStorePath())
                .tlsTrustStoreType(configData.getTlsTrustStoreType())
                .allowTlsInsecureConnection(configData.isAllowTlsInsecureConnection())
                .enableTlsHostnameVerification(configData.isEnableTlsHostnameVerification());

        return pulsarClientBuilder.build();

    }
}
