package com.ordering.system.pulsar.producer.service;


public interface PulsarProducer<T> {

    void publish(T message);
}
