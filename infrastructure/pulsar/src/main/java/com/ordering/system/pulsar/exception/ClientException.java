package com.ordering.system.pulsar.exception;

import java.io.IOException;

public class ClientException extends IOException {
    public ClientException(String message) {
        super(message);
    }

    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
