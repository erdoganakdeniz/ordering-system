package com.ordering.system.model;

public enum OrderStatus {
    CREATED, PAID, SHIPPED, CANCELLING, CANCELLED, DELIVERED;

}
