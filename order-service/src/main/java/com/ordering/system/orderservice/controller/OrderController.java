package com.ordering.system.orderservice.controller;


import com.ordering.system.model.OrderStatus;
import com.ordering.system.orderservice.dto.OrderDTO;
import com.ordering.system.orderservice.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/order")
@Tag(name = "Order API")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<OrderDTO> placeOrder(@RequestBody OrderDTO order) throws PulsarClientException {
        return ResponseEntity.ok(orderService.placeOrder(order));
    }
    @GetMapping("/{orderId}")
    public ResponseEntity<OrderDTO> getOrderByOrderId(@PathVariable UUID orderId){
        return ResponseEntity.ok(orderService.getOrderByOrderId(orderId));
    }
    @PutMapping("/{orderId}/update-status")
    public ResponseEntity<Optional<OrderDTO>> updateOrderStatusByOrderId(@PathVariable("orderId") UUID orderId, @RequestParam("orderStatus") OrderStatus orderStatus) {
        Optional<OrderDTO> updatedOrder = orderService.updateOrderStatusByOrderId(orderId, orderStatus);
        return ResponseEntity.ok(updatedOrder);
    }

}
