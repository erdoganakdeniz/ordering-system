package com.ordering.system.orderservice.entity;


import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;
import java.util.UUID;


@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Document(indexName = "order_item")
public class OrderItem {

    private UUID orderItemId=UUID.randomUUID();
    private UUID productId;
    private int quantity;
    private BigDecimal price;
    private BigDecimal subTotal;


    public void calculateSubTotal() {
        subTotal = price.multiply(BigDecimal.valueOf(quantity));
    }
}
