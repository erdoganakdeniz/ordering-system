package com.ordering.system.orderservice.entity;

import com.ordering.system.model.OrderStatus;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Builder
@RequiredArgsConstructor
@Getter
@Setter
@ToString
@AllArgsConstructor
@Document(indexName = "order")
public class Order {

    @Id
    private UUID orderId;
    private OrderStatus orderStatus;
    private UUID deliveryAddressId;
    private List<OrderItem> orderItems;
    private BigDecimal totalPrice;
    private UUID customerId;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}
