package com.ordering.system.orderservice.service;


import com.ordering.system.model.OrderStatus;
import com.ordering.system.orderservice.dto.OrderDTO;
import org.apache.pulsar.client.api.PulsarClientException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {

    OrderDTO placeOrder(OrderDTO orderDto) throws PulsarClientException;
    List<OrderDTO> getAllOrder();
    OrderDTO getOrderByOrderId(UUID orderId);

    OrderDTO cancelOrder(UUID orderId);
    Optional<OrderDTO> updateOrderStatusByOrderId(UUID orderId, OrderStatus orderStatus);

}
