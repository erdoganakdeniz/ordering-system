package com.ordering.system.orderservice.publisher;


import com.ordering.system.pulsar.producer.service.PulsarProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CreateOrderMessagePublisher<OrderDTO> implements PulsarProducer<OrderDTO> {

    @Override
    public void publish(OrderDTO message) {

    }


}
