package com.ordering.system.orderservice.repository;


import com.ordering.system.orderservice.entity.Order;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.UUID;


public interface OrderRepository extends ElasticsearchRepository<Order, UUID> {
}
