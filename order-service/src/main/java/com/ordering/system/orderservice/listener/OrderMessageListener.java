package com.ordering.system.orderservice.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ordering.system.orderservice.dto.OrderDTO;
import com.ordering.system.orderservice.mapper.OrderDataMapper;
import com.ordering.system.orderservice.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.pulsar.client.api.SubscriptionType;
import org.apache.pulsar.common.schema.SchemaType;
import org.springframework.pulsar.annotation.PulsarListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderMessageListener {
    private final OrderRepository orderRepository;
    private final OrderDataMapper orderDataMapper;

    public OrderMessageListener(OrderRepository orderRepository, OrderDataMapper orderDataMapper) {
        this.orderRepository = orderRepository;
        this.orderDataMapper = orderDataMapper;
    }

    @PulsarListener(
            topics = "${spring.pulsar.producer.topic-name}",
            subscriptionName = "order-subscription",
            schemaType = SchemaType.JSON,
            subscriptionType = SubscriptionType.Shared
    )
    public void consumeOrderMessage(OrderDTO orderDTO) throws JsonProcessingException {
        orderRepository.save(orderDataMapper.createOrderDTOtoOrder(orderDTO));
        log.info("EventConsumer:: consumeTextEvent consumed events {}", new ObjectMapper().writeValueAsString(orderDTO));
    }
}
