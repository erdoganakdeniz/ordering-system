package com.ordering.system.orderservice.service;


import com.ordering.system.model.OrderStatus;
import com.ordering.system.orderservice.dto.OrderDTO;
import com.ordering.system.orderservice.entity.Order;
import com.ordering.system.orderservice.exception.NotFoundException;
import com.ordering.system.orderservice.mapper.OrderDataMapper;
import com.ordering.system.orderservice.publisher.CreateOrderMessagePublisher;
import com.ordering.system.orderservice.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderDataMapper orderDataMapper;
    private final CreateOrderMessagePublisher messagePublisher;

    public OrderServiceImpl(OrderRepository orderRepository, OrderDataMapper orderDataMapper, CreateOrderMessagePublisher messagePublisher) {

        this.orderRepository = orderRepository;
        this.orderDataMapper = orderDataMapper;
        this.messagePublisher = messagePublisher;
    }

    @Override
    public OrderDTO placeOrder(OrderDTO orderDto) {

        Order order = orderDataMapper.createOrderDTOtoOrder(orderDto);
        order.setOrderId(UUID.randomUUID());
        order.setOrderItems(order.getOrderItems());
        order.setTotalPrice(calculateTotalPrice(order));
        order.setOrderStatus(OrderStatus.CREATED);
        messagePublisher.publish(orderDataMapper.orderToOrderDTO(order));
        log.info("Order created: {}", orderDto);
        return orderDto;
    }
    @Override
    public List<OrderDTO> getAllOrder() {
        Function<Order, OrderDTO> mapper = orderDataMapper::orderToOrderDTO;
        Stream<Order> orderStream = StreamSupport.stream(orderRepository.findAll().spliterator(), false);
        return orderStream.map(mapper).toList();
    }

    @Override
    public OrderDTO getOrderByOrderId(UUID orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Order not found!! with given orderId : " + orderId));
        return orderDataMapper.orderToOrderDTO(order);
    }

    @Override
    public OrderDTO cancelOrder(UUID orderId) {
        log.info("Order is cancelled with id: {}", orderId);
        return updateOrderStatusByOrderId(orderId, OrderStatus.CANCELLED).orElseThrow();
    }

    @Override
    public Optional<OrderDTO> updateOrderStatusByOrderId(UUID orderId, OrderStatus orderStatus) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Order not found!! with given orderId : " + orderId));
        order.setOrderStatus(orderStatus);

        log.info("Order status is updated with id: {}", orderId);
        return Optional.of(orderDataMapper.orderToOrderDTO(orderRepository.save(order)));
    }

    private BigDecimal calculateTotalPrice(Order order) {
        return order.getOrderItems().stream().map(orderItem -> {
            orderItem.calculateSubTotal();
            return orderItem.getSubTotal();
        }).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
