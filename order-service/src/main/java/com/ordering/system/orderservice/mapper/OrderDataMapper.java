package com.ordering.system.orderservice.mapper;

import com.ordering.system.orderservice.dto.OrderDTO;
import com.ordering.system.orderservice.dto.OrderItemDTO;
import com.ordering.system.orderservice.entity.Order;
import com.ordering.system.orderservice.entity.OrderItem;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderDataMapper {

    public Order createOrderDTOtoOrder(OrderDTO orderDTO){
        return Order.builder().customerId(orderDTO.customerId())
                .deliveryAddressId(orderDTO.deliveryAddressId())
                .orderItems(orderItemDTOListToOrderItemList(orderDTO.orderItems()))
                .orderStatus(orderDTO.orderStatus()).build();
    }
    public OrderDTO orderToOrderDTO(Order order){
        return new OrderDTO(order.getOrderId(),order.getCustomerId(),
                order.getDeliveryAddressId(),
                orderItemListToOrderItemDTOList(order.getOrderItems()),
                order.getTotalPrice(),
                order.getOrderStatus()
                );
    }

    public List<OrderItem> orderItemDTOListToOrderItemList(List<OrderItemDTO> orderItemDTOList) {
        return orderItemDTOList.stream()
                .map(this::mapToOrderItem)
                .toList();
    }

    public OrderItem mapToOrderItem(OrderItemDTO orderItemDTO) {
        return OrderItem.builder().productId(orderItemDTO.productId())
                .price(orderItemDTO.price())
                .quantity(orderItemDTO.quantity())
                .build();
    }

    public List<OrderItemDTO> orderItemListToOrderItemDTOList(List<OrderItem> orderItemList) {
        return orderItemList.stream()
                .map(this::mapToOrderItemDTO)
                .toList();
    }
    public OrderItemDTO mapToOrderItemDTO(OrderItem orderItem) {
        return new OrderItemDTO(orderItem.getProductId(),
                orderItem.getQuantity(),
                orderItem.getPrice());
    }




}
