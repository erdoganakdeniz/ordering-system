package com.ordering.system.orderservice.dto;

import com.ordering.system.model.OrderStatus;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public record OrderDTO(UUID orderId, UUID customerId, UUID deliveryAddressId, List<OrderItemDTO> orderItems,BigDecimal totalPrice, OrderStatus orderStatus) {
}
