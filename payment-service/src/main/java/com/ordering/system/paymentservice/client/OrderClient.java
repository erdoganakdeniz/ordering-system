package com.ordering.system.paymentservice.client;


import com.ordering.system.model.OrderStatus;
import com.ordering.system.paymentservice.dto.OrderDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.UUID;

@FeignClient(name = "order-service",url = "localhost:8080/api/v1/order")
public interface OrderClient {

    @GetMapping("/{orderId}")
    ResponseEntity<OrderDTO> getOrderByOrderId(@PathVariable UUID orderId);

    @PutMapping("/{orderId}/update-status")
    ResponseEntity<Optional<OrderDTO>> updateOrderStatusByOrderId(@PathVariable("orderId") UUID orderId, @RequestParam("orderStatus") OrderStatus orderStatus);



}
