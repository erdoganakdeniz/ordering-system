package com.ordering.system.paymentservice.service;


import com.ordering.system.model.OrderStatus;
import com.ordering.system.paymentservice.client.OrderClient;
import com.ordering.system.paymentservice.entity.Payment;
import com.ordering.system.paymentservice.exception.PaymentNotFoundException;
import com.ordering.system.paymentservice.repository.PaymentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PaymentServiceImpl implements PaymentService{

    private final PaymentRepository paymentRepository;

    private final OrderClient orderClient;

    public PaymentServiceImpl(PaymentRepository paymentRepository, OrderClient orderClient) {
        this.paymentRepository = paymentRepository;
        this.orderClient = orderClient;
    }

    @Override
    public Payment processPayment(Payment payment) {
        orderClient.updateOrderStatusByOrderId(payment.getOrderId(), OrderStatus.PAID);
        return paymentRepository.save(payment);

    }

    @Override
    public List<Payment> getAllPayment() {
        return paymentRepository.findAll();
    }

    @Override
    public Payment getPaymentByPaymentId(UUID paymentId) {
        return paymentRepository.findById(paymentId)
                .orElseThrow(()->new PaymentNotFoundException("Payment not found!! with given paymentId:"+paymentId));
    }
}
