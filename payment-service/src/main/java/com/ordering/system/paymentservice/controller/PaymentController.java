package com.ordering.system.paymentservice.controller;


import com.ordering.system.paymentservice.entity.Payment;
import com.ordering.system.paymentservice.service.PaymentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/payment")
@Tag(name = "Payment API")
public class PaymentController {

    private final PaymentService paymentService;


    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }
    @PostMapping
    public ResponseEntity<Payment> processPayment(@RequestBody Payment payment){
        return ResponseEntity.ok(paymentService.processPayment(payment));
    }
    @GetMapping("/{paymentId}")
    public ResponseEntity<Payment> getPaymentByPaymentId(@PathVariable UUID paymentId){
        return ResponseEntity.ok(paymentService.getPaymentByPaymentId(paymentId));
    }
}
