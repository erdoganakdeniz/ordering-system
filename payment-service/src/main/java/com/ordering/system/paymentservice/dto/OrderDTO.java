package com.ordering.system.paymentservice.dto;

import com.ordering.system.model.OrderStatus;

import java.util.List;
import java.util.UUID;

public record OrderDTO(UUID customerId, UUID deliveryAddressId, List<OrderItemDTO> orderItems, OrderStatus orderStatus) {
}
