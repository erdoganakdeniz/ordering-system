package com.ordering.system.paymentservice.service;


import com.ordering.system.paymentservice.entity.Payment;

import java.util.List;
import java.util.UUID;

public interface PaymentService {

    Payment processPayment(Payment payment);
    List<Payment> getAllPayment();
    Payment getPaymentByPaymentId(UUID paymentId);
}
